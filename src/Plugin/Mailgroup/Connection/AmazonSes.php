<?php

namespace Drupal\mailgroup_amazonses\Plugin\Mailgroup\Connection;

use Aws\S3\Exception\S3Exception;
use Drupal\aws\Traits\AwsClientFactoryTrait;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\mailgroup\ConnectionBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Amazon SES connection plugin.
 *
 * @Connection(
 *   id = "amazonses",
 *   label = @Translation("Amazon SES")
 * )
 */
class AmazonSes extends ConnectionBase implements ContainerFactoryPluginInterface {

  use AwsClientFactoryTrait;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return (new static($configuration, $plugin_id, $plugin_definition))
      ->setAwsClientFactory($container->get('aws.client_factory'));
  }

  /**
   * {@inheritdoc}
   */
  public function getFields() {
    $config = $this->configuration;

    $fields['bucket'] = [
      '#type' => 'textfield',
      '#title' => $this->t('S3 Bucket'),
      '#description' => $this->t('The S3 bucket email are stored in.'),
      '#default_value' => $config['bucket'] ?? NULL,
    ];

    $fields['delete'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Delete after retrieval'),
      '#description' => $this->t('Delete messages from S3 after retrieval.'),
      '#default_value' => $config['delete'] ?? TRUE,
    ];

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function testConnection() {
    /** @var \Aws\S3\S3ClientInterface $client */
    $client = $this->awsClientFactory->getClient('s3');

    try {
      $client->listObjects([
        'Bucket' => $this->configuration['bucket'],
        'MaxKeys' => 1,
      ]);

      return TRUE;
    }

    catch (S3Exception $e) {
      return FALSE;
    }
  }

}
