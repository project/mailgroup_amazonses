<?php

namespace Drupal\mailgroup_amazonses;

use Aws\S3\S3Client;

/**
 * Factory class for AWS S3Client instances.
 */
class S3ClientFactory implements S3ClientFactoryInterface {

  /**
   * Default S3 client options.
   *
   * @var array
   */
  protected $clientOptions;

  /**
   * Set the default client options.
   *
   * @param array $options
   *   An array of option values.
   *
   * @return $this
   */
  public function setClientOptions(array $options) {
    $this->clientOptions = $options;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function create(array $config) {
    $options = array_merge($config, $this->clientOptions);

    return new S3Client($options);
  }

}
