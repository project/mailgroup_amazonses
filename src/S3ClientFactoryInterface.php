<?php

namespace Drupal\mailgroup_amazonses;

/**
 * Interface for defining an S3 client factory.
 */
interface S3ClientFactoryInterface {

  /**
   * Creates an AWS SesClient instance.
   *
   * @param array $config
   *   An array of configuration values.
   *
   * @return \Aws\S3\S3Client
   *   The client instance.
   */
  public function create(array $config);

}
