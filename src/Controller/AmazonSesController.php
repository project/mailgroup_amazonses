<?php

namespace Drupal\mailgroup_amazonses\Controller;

use Aws\S3\Exception\S3Exception;
use Aws\Sns\Message as SnsMessage;
use Aws\Sns\MessageValidator;
use Drupal\aws\Traits\AwsClientFactoryTrait;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\mailgroup\Entity\MailGroupInterface;
use Drupal\mailgroup_amazonses\Plugin\Mailgroup\Connection\AmazonSes;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;
use ZBateson\MailMimeParser\Message;

/**
 * Controller for Mail Group Amazon SES routes.
 */
class AmazonSesController extends ControllerBase {

  use AwsClientFactoryTrait;
  use StringTranslationTrait;

  /**
   * The Guzzle HTTP client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return (new static())
      ->setAwsClientFactory($container->get('aws.client_factory'))
      ->setHttpClient($container->get('http_client'))
      ->setLogger($container->get('logger.channel.mailgroup_amazonses'));
  }

  /**
   * Set the HTTP client.
   *
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The Guzzle HTTP client.
   *
   * @return $this
   */
  protected function setHttpClient(ClientInterface $http_client) {
    $this->httpClient = $http_client;
    return $this;
  }

  /**
   * Set the logger.
   *
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger
   *   The logger channel.
   *
   * @return $this
   */
  protected function setLogger(LoggerChannelInterface $logger) {
    $this->logger = $logger;
    return $this;
  }

  /**
   * Respond to POST requests.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   An empty response.
   */
  public function receive() {
    $message = SnsMessage::fromRawPostData();
    $validator = new MessageValidator();

    if ($validator->isValid($message)) {
      if ($message['Type'] == 'Notification') {
        /** @var \Drupal\mailgroup\Entity\Storage\MailGroupStorageInterface $group_storage */
        $group_storage = $this->entityTypeManager()->getStorage('mailgroup');
        $message_data = json_decode($message['Message']);
        $recipients = $message_data->mail->destination;

        foreach ($recipients as $recipient) {
          $group = $group_storage->loadByEmail($recipient);

          if ($group) {
            $message_id = $message_data->mail->messageId;
            $this->createMessage($group, $message_id);
          }
        }
      }
      elseif ($message['Type'] == 'SubscriptionConfirmation') {
        $this->confirm($message['SubscribeURL']);
      }
    }

    return new Response();
  }

  /**
   * Create a Mail Group Message.
   *
   * @param \Drupal\mailgroup\Entity\MailGroupInterface $group
   *   The group to create the message in.
   * @param string $message_id
   *   The ID of the received message.
   */
  protected function createMessage(MailGroupInterface $group, $message_id) {
    $plugin = $group->getConnectionPlugin();
    if ($plugin instanceof AmazonSes) {
      $config = $plugin->getConfig();

      /** @var \Aws\S3\S3ClientInterface $client */
      $client = $this->awsClientFactory->getClient('s3');

      try {
        $result = $client->getObject([
          'Bucket' => $config['bucket'],
          'Key' => $message_id,
        ]);
        $body = $result->get('Body');
        $mail = Message::from($body, TRUE);

        /** @var \Drupal\mailgroup\Entity\Storage\MailGroupMembershipStorageInterface $membership_storage */
        $membership_storage = $this->entityTypeManager()->getStorage('mailgroup_membership');
        $message_storage = $this->entityTypeManager()->getStorage('mailgroup_message');

        /** @var \ZBateson\MailMimeParser\Header\AddressHeader $from_header */
        $from_header = $mail->getHeader('From');
        $subject_header = $mail->getHeader('Subject');

        // Ignore emails from the SES mailer daemon.
        $from_email = $from_header->getEmail();
        if ($from_email == 'MAILER-DAEMON@amazonses.com') {
          return;
        }

        $memberships = $membership_storage->loadByEmail($from_email, $group);
        $sending_member = reset($memberships);

        $values = [
          'gid' => $group->id(),
          'uid' => $sending_member->getOwnerId(),
          'subject' => $subject_header->getValue(),
          'body' => $mail->getTextContent(),
        ];
        $message = $message_storage->create($values);

        try {
          $message->save();

          if ($config['delete']) {
            $client->deleteMatchingObjects($config['bucket'], $message_id);
          }
        }
        catch (EntityStorageException $e) {
          $this->logger->error($e->getMessage());
          $this->logger->error($this->t('Unable to save message to @group', ['@group' => $group->getName()]));
        }
      }
      catch (S3Exception $e) {
        $this->logger->error($e->getMessage());
        $this->logger->error($this->t('Unable to create the message.'));
      }
    }
  }

  /**
   * Confirm a subscription to a SNS topic.
   *
   * @param string $subscribe_url
   *   The subscribe URL to send a request to.
   */
  protected function confirm($subscribe_url) {
    try {
      $response = $this->httpClient->request('GET', $subscribe_url);
      $code = $response->getStatusCode();

      if ($code == 200) {
        $this->logger->info($this->t('Subscription to SNS confirmed.'));
      }
      else {
        $this->logger->error($this->t('Unable to confirm subscription to SNS.'));
      }
    }
    catch (GuzzleException $e) {
      $this->logger->error($this->t('Unable to confirm subscription to SNS.'));
    }

  }

}
